Introduction:
-------------
Image processing using Neural Network

This project puts an emphasis on the math behind Stochastic Gradient Descent and Back Propagation.
For this purpose: 
    - Packages supporting deep learning such as Keras, Tensorflow are not used.
    - The network has only 1 hidden layer so as to increase the performance.

Structure of the network:
-------------------------
    Input: 784 nodes - flattened array of an the pixel value of an 28x28 image
    Hidden layer: 
        1 hidden layer of 10 nodes
    Output: 10 nodes - binary label of the image. 
        For example: the number 2 will have an output as [0,0,1,0,0,0,0,0,0,0]

Data: 
-----
This Network learns to predict image in the MNIST dataset

Run:
----
Run the run.py. Result of each epoch will be printed in the terminal.

How to interprete the result:
-----------------------------
"Epoch no: correctly classified images/total images"
