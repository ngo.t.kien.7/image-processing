import numpy as np
import random
def sigmoid(z):
        return 1.0/(1.0 + np.exp(-z))
def sigmoid_prime(z):
    return sigmoid(z)*(1-sigmoid(z))
class Network(object):
    def __init__(self, sizes):
        """
        Initiate attributes of the network

        Parameters:
        -----------
        sizes: list
            Size of each layer of the network
        """
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(x,y) for x,y in zip(sizes[1:],sizes[:-1])]
    
    def feedforward(self, a):
        """
        Calculate the output of the network given the input image
        Parameters:
        a: numpy array - Shape (w*h,1)
            the flatened array of the input image. 
        Return:
        a: numpy array
            The output of the network
        """
        for w,b in zip(self.weights, self.biases):
            a = sigmoid(np.dot(w,a) + b)
        return a
    def cost_derivative(self, output_activations,y):
        """
        Calculate the cost of an output node
        Parameters:
        -----------
        output_activations: numpy array - Shape(10,1)
            activation output of the last layer
        y: numpy array - Shape(10,1)
            binary label of the image

        Return:
        -------
            Cost of the activation output
        """
        return (output_activations - y)
    def evaluate(self, test_data):
        """
        Calculate the test accuracy
        Parameters:
        ----------
        test_data: list 
            List of data for the test images. Read description on load_data.py
        Return:
        -------
            Total number of accurate outputs
        """
        test_results = [(np.argmax(self.feedforward(x)),y) for (x,y) in test_data]
        return sum(int(x==y) for (x,y) in test_results)
    def backprop(self,x,y):
        """
        Applying backpropagation on the network
        Parameters:
        -----------
        x: numpy array- Shape (w*h,1)
            the flatened array of the input image. 

        y: numpy array
            binary label of the image
        
        Return:
        -------
            nabla_b: List
                List of changes to be applied to biases in each layers
            nabla_w: List
                List of changes to be applied to weights in each layers

        """
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(b.shape) for b in self.weights]
##        Phải khởi tạo là zero matrix để có thể zip vào self weight và self biases
        activation = x
        activations = [x]
        zs = []
        for w,b in zip(self.weights, self.biases):
            
            z = np.dot(w,activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
            ##  Backward pass
        delta = self.cost_derivative(activations[-1], y)*sigmoid_prime(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())
        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l +1].transpose(), delta)*sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l -1].transpose())
            return(nabla_b, nabla_w)
    def update_mini_batch(self, mini_batch, eta):
        """
        Feed the network with a mini batch and update the its weights and biases
        Parameters:
        -----------
        mini_batch: List
            List of tuples containing 
            1. the flatened array of the input image. 
            2. the image's binary label
        
        eta: int
            The learning rate of gradient descent
        
        Return:
        -------
        None
            The function update the weight and bias attributes of the object and does not return anything
        """
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        for x,y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x,y)
            nabla_b = [nb + dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nb + dnb for nb, dnb in zip(nabla_w, delta_nabla_w)]
        self.weights = [w - (eta/len(mini_batch))*nw for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b - (eta/len(mini_batch))*nb for b, nb in zip(self.biases, nabla_b)]

    def SGD(self, training_data, epochs, mini_batch_size, eta, test_data = None):
        """
        Applying Stochastic Gradient Descent for optimization
        Parameters:
        -----------
        training_data: list
            List of tuples containing 
            1. the flatened array of the input image. 
            2. the image's binary label
        epochs: int
            Number of time the training data is fed to the network
        mini_batch_size
            Size of each batch (in stochastic gradient descent, training data is divided into smaller batches)
        eta:
            The learning rate
        test_data:
            List of tuples containing 
            1. the flatened array of the input image. 
            2. the image's decimal label 
        """
        if test_data:
            n_test = len(test_data)
        n = len(training_data)
        for j in range(epochs):
            random.shuffle(training_data)
            mini_batches = [training_data[k: k+ mini_batch_size] for k in range(0,n,mini_batch_size)]
            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, eta)
            if test_data:
                print(f"Epoch {j + 1}: {self.evaluate(test_data)} / {n_test}")
            else:
                print(f"Epoch {j + 1} complete")
                      
    
            
    
# myObj = Network([2,3,2])

# x = np.array([2,1])
# x = x.reshape(2,1)
# y = np.array([1,0])
# y = y.reshape(2,1)
# b = myObj.backprop(x,y)
